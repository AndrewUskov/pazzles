var socket = require('socket.io');
var express = require('express');
var app = express();
var io = socket.listen(app.listen(8080, '127.0.0.1'));
var rightAnswer = require('./answers-bd.json');

var rightAnswerLength = 0;
for (var key in rightAnswer) {
    rightAnswerLength = ++key;
}

app.get('/', function (req, res) {
    res.sendfile(__dirname + '/puzzles.html');
});

io.sockets.on('connection', function (client) {
   console.log('Connected');
   var i = 0, countRightAnswer = 0;

    client.on('answer', function (userAnswer) {
        if (userAnswer === '') {
            client.emit('answer', {answer: 'Пустое поле'});
        } else if (userAnswer !== rightAnswer[i]) {
            client.emit('answer', {answer: 'Неверно'});
        } else {
            countRightAnswer++;
            client.emit('answer', {answer: 'Верно'});
        }

        i++;
        if(i === rightAnswerLength) {
            i = 0;
            client.emit('count', {count: countRightAnswer});
            countRightAnswer = 0;
        }
    })
});